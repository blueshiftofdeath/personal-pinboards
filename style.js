let allPinIds = {}

function initPinTouch() {
  var pins = document.getElementsByClassName("pin");
  for (var i = 0; i < pins.length; i++) {
    var pin = pins[i];
    var pinLink = pin.getElementsByTagName('a')[0].href;
    var pinText = pin.getElementsByTagName('p')[0].innerHTML;
    var overlayTouchBlock = pin.querySelector('.overlayTouch');
    var overlayTouchLink = overlayTouchBlock.getElementsByTagName('a')[0];
    var overlayTouchText = overlayTouchBlock.getElementsByTagName('p')[0];
    overlayTouchLink.href = pinLink;
    overlayTouchText.innerHTML = pinText;
    allPinIds[pin.id] = false;
  }
}

initPinTouch() 

function turnOffOverlayTouch(pinId) {
  var pin = document.getElementById(pinId);
  var overlayTouchBlock = pin.querySelector('.overlayTouch');
  overlayTouchBlock.style.display = "none";
  allPinIds[pin.id] = false;
} 

function turnOnOverlayTouch(pinId) {
  var pin = document.getElementById(pinId);
  var overlayTouchBlock = pin.querySelector('.overlayTouch');
  overlayTouchBlock.style.display = "block";
  allPinIds[pin.id] = true;
} 

function toggleOverlayTouch(pinId) {
  var pin = document.getElementById(pinId);
  if (window.matchMedia('only screen and (hover: none)').matches) {
    var overlayTouchBlock = pin.querySelector('.overlayTouch');
    if (allPinIds[pin.id] === false) {
      for (var otherPinId in allPinIds) {
        turnOffOverlayTouch(otherPinId);
      }
      turnOnOverlayTouch(pinId);
    } else {
      turnOffOverlayTouch(pinId);
    }
  }
} 